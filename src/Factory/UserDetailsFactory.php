<?php

declare(strict_types=1);

namespace App\Factory;

use App\Entity\User;
use App\Entity\UserDetails;

class UserDetailsFactory
{
    public function createMaleUserDetails(): UserDetails
    {
       return  $this->createUser('red', 20, 'male');
    }

    private function createUser(string $color, int $age, string $sex): UserDetails
    {
        $userDetails = new UserDetails('red', 20);
        $userDetails->setSex($sex);
        return $userDetails;
    }
}
