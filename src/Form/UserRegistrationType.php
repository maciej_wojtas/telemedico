<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\User;
use App\Form\UserDetailsType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class UserRegistrationType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName')
            ->add('lastName')
            ->add('email', EmailType::class)
            ->add('plainPassword', PasswordType::class, ['mapped' => false, 'label' => 'password',
                'constraints' => array(
                    new NotBlank(
                        ['message' => 'please enter password']),
                    new Length(['min' => 8, 'minMessage' => 'password must contain at least 8 characters'])
                )]);

            $builder->add('sex', UserDetailsType::class,

            );
        ;
    }

    /**
     * {@inheritdoc}
     */

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }



}
