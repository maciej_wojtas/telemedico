
# features/users.feature
Feature: Users feature
  Scenario: Calling api for listing users
    When I add "Content-Type" header equal to "application/json"
    And I add "Authorization" header equal to "Bearer 6875ecf835a832d4d997e4c42b06f0e5"
    And I send a "GET" request to "/api/users"
    Then the response status code should be 200
    And the response should be in JSON
    And the header "Content-Type" should be equal to "application/json"

#    to be continued
