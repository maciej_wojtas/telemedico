<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use PHPUnit\Framework\TestCase;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class UserControllerTest extends WebTestCase {

    protected $userId;
    protected $client;
    protected $alidToken;
    protected $invalidToken;
    protected $apiPath;
    protected $validPass;
    protected $invalidPass;
    protected $invalidData;
    protected $validData;

    public function setUp() {
        $this->client = static::createClient();
        $this->validToken = '6875ecf835a832d4d997e4c42b06f0e5';
        $this->invalidToken = 'a49a8b852ce2938b8f438b830ea20892';
        $this->apiPath = '/api/users';
        $this->validPass = 'maciejmaciej';
        $this->invalidPass = 'maciejmaciej';
        $this->userId = 5;

        $this->invalidData = [
            'email' => rand(1, 100) . "@maciej.pl",
            'firstName' => "John",
            'lastName' => "Doe",
            'plainPassword' => 'symf'
        ];
        $this->validData = [
            'email' => rand(1, 100) . "@maciej.pl",
            'firstName' => "John",
            'lastName' => "Doe",
            'plainPassword' => 'symfony4'
        ];
    }

    public function testPOSTUserValidData() {
        $this->client->request('POST', $this->apiPath , ['User' => $this->validData], [], array('CONTENT_TYPE' => 'application/json',
            'HTTP_AUTHORIZATION' => "Bearer " . $this->validToken));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPOSTUserInvalidData() {
        $this->client->request('POST', $this->apiPath , ['User' => $this->invalidData], [], array('CONTENT_TYPE' => 'application/json',
            'HTTP_AUTHORIZATION' => "Bearer " . $this->validToken));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());

        $jsonResponseData = json_decode($this->client->getResponse()->getContent(), true);
        $this->assertArrayHasKey('errors', $jsonResponseData);
    }

    public function testInvalidToken() {

        $this->client->request('GET', $this->apiPath, array(), array(), array('CONTENT_TYPE' => 'application/json',
            'HTTP_AUTHORIZATION' => "Bearer " . $this->invalidToken));
        $response = $this->client->getResponse();
        $this->assertEquals(401, $response->getStatusCode());
    }

    public function testGETUserValidData() {
        $this->client->request('GET', $this->apiPath . '/' . $this->userId, array(), array(), array('CONTENT_TYPE' => 'application/json',
            'HTTP_AUTHORIZATION' => "Bearer " . $this->validToken));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGETUsers() {
        $this->client->request('GET', $this->apiPath, array(), array(), array('CONTENT_TYPE' => 'application/json',
            'HTTP_AUTHORIZATION' => "Bearer " . $this->validToken));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPUTUserInvalidData() {

        $this->client->request('PUT', $this->apiPath . '/' . $this->userId, ['User' => $this->invalidData], [], array('CONTENT_TYPE' => 'application/json',
            'HTTP_AUTHORIZATION' => "Bearer " . $this->validToken));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());

        $jsonResponseData = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('type', $jsonResponseData);
        $this->assertArrayHasKey('title', $jsonResponseData);
        $this->assertArrayHasKey('errors', $jsonResponseData);
    }

    public function testPUTUserValidData() {

        $this->client->request('PUT', $this->apiPath . '/' . $this->userId, ['User' => $this->validData], [], array('CONTENT_TYPE' => 'application/json',
            'HTTP_AUTHORIZATION' => "Bearer " . $this->validToken));
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testDELETEUser() {

        $this->client->request('DELETE', $this->apiPath . '/' . $this->userId, [], [], array('CONTENT_TYPE' => 'application/json',
            'HTTP_AUTHORIZATION' => "Bearer " . $this->validToken));
        $response = $this->client->getResponse();
        $this->assertEquals(204, $response->getStatusCode());
    }

    public function testGETUserInvalidData() {
        $this->client->request('GET', $this->apiPath . '/' . $this->userId, array(), array(), array('CONTENT_TYPE' => 'application/json',
            'HTTP_AUTHORIZATION' => "Bearer " . $this->validToken));
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
        $jsonResponseData = json_decode($this->client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('type', $jsonResponseData);
        $this->assertArrayHasKey('title', $jsonResponseData);
    }

}
