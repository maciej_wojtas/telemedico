<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\UserAuthToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method getAllUserTokens[]    findAll()
 */
class UserAuthTokenRepository extends ServiceEntityRepository implements UserAuthTokenRepositoryInterface
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserAuthToken::class);
    }

    /**
     * @return UserAuthToken[] Returns an array of UserAuthToken objects
     */
    public function getAllUserTokens(int $userId): array
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.User = :val')
            ->setParameter('val', $userId)
            ->orderBy('u.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


}
