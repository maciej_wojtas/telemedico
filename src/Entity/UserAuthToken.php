<?php

declare(strict_types=1);

namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;


class UserAuthToken
{
    /** @var int */

    private $id;

    /** @var UserInterface */

    private $User;

    /** @var string */

    private $token;

    /**@var \DateTime */

    private $expiresAt;

    public function __construct(User $user)
    {
        $this->token = md5(uniqid( strval(rand()) , true) . 'salt');
        $this->User = $user;
        $this->expiresAt = new \DateTime('+10 hour');

    }

    public function isExpired(): bool
    {
        return $this->expiresAt <= new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getExpiresAt(): ?\DateTimeInterface
    {
        return $this->expiresAt;
    }

    public function setExpiresAt(\DateTimeInterface $expiresAt): self
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }
}
