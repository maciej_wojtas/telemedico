<?php

namespace App\Security;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use App\Repository\UserAuthTokenRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;

class ApiTokenAuthenticator extends AbstractGuardAuthenticator {

    private $userAuthTokenRepository;

    public function __construct(UserAuthTokenRepository $userAuthTokenRepository) {
        $this->userAuthTokenRepository = $userAuthTokenRepository;
    }

    public function supports(Request $request) {
        return $request->headers->has('Authorization') && 0 === strpos($request->headers->get('Authorization'), 'Bearer');
    }

    public function getCredentials(Request $request) {
        $token = $request->headers->get('Authorization');
        return substr($token, 7);
    }

    public function getUser($credentials, UserProviderInterface $userProvider) {

        $token = $this->userAuthTokenRepository->findOneBy([
            'token' => $credentials
        ]);
        if (!$token) {
            throw new CustomUserMessageAuthenticationException('invalid token');
        }
        if ($token->isExpired()) {
            throw new CustomUserMessageAuthenticationException('token Expired');
        }

        return $token->getUser();
    }

    public function checkCredentials($credentials, UserInterface $user) {
        return true;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception) {
        return new JsonResponse(['message' => $exception->getMessageKey()], 401);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey) {
    }

    public function start(Request $request, AuthenticationException $authException = null) {
        // todo
    }

    public function supportsRememberMe() {
        // todo
    }

}
