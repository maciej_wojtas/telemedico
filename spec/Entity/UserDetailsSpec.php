<?php

namespace spec\App\Entity;

use App\Entity\UserDetails;
use PhpSpec\Exception\Example\FailureException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UserDetailsSpec extends ObjectBehavior
{
    function getMatchers(): array
    {
        return [
            'returnUndefined' => function ($subject) {
                if ($subject !== 'undefined') {
                    throw new FailureException(sprintf('expected value of string undefined but got %s', $subject));
                }
                return true;
            }
        ];
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(UserDetails::class);
    }


    function it_should_default_to_undefined_color()
    {
        $this->getColor()->shouldReturn('undefined');
    }

    function it_should_return_age()
    {
        $this->getAge()->shouldReturn(0);
    }

    function it_should_return_user_details_for_maciej()
    {
        $this->beConstructedWith('blue', 23);
        $this->getDescription()->shouldReturn('Users color is blue and age is 23 ');
    }

    function it_should_allow_to_set_color()
    {
        $this->setColor('blue');
        $this->getColor()->shouldReturn('blue');
    }

    function it_should_allow_to_set_sex()
    {
        $this->setSex('male');
        $this->getSex()->shouldReturn('male');
    }

    function it_should_build_marek_user_details()
    {
        $this->beConstructedThrough('buildDefaultUser', ['male']);

    }

    function it_should_be_male_by_default()
    {
        $this->shouldBeMale();
    }

    function it_should_allow_to_check_if_user_is_male()
    {
        $this->beConstructedWith('red', 30, 'female');
        $this->shouldNotBeMale();
    }
}
