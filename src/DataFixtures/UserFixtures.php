<?php

declare(strict_types=1);

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\UserAuthToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    public function load(ObjectManager $manager): void
    {
        $connection = $manager->getConnection();
        $connection->exec("ALTER TABLE user AUTO_INCREMENT = 1;");
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setPassword('$2y$13$qmEs5EmwYl2OA0tWcS3qI.FhkQLG8lgAUznnrLFdh.jL0BpzTRxdS');
            $user->setEmail('example' . $i . '@example.pl');
            $user->setFirstName('John');
            $user->setLastName('Doe');
            $userAuthToken = new UserAuthToken($user);
            $userAuthToken->setToken('6875ecf835a832d4d997e4c42b06f0e5');
            $manager->persist($user);
            $manager->persist($userAuthToken);
        }
        $manager->flush();
    }

}
