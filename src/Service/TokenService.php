<?php

declare(strict_types=1);


namespace App\Service;


use App\Repository\UserAuthTokenRepository;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;


class TokenService
{
    public function __construct(EntityManagerInterface $em, UserAuthTokenRepository $userTokenRepo)
    {
        $this-> userTokenRepo = $userTokenRepo;
        $this->em = $em;
    }

    public function removeAllUserTokens(User $user) : void
    {
        $tokens = $this->userTokenRepo->getAllUserTokens($user->getId());
        foreach ($tokens as $token) {
            $this->em->remove($token);
        }

    }

}
