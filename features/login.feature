
# features/users.feature
Feature: login feature

  Background:
  Given I am on "/login"

Scenario Outline:
  When I fill in "email" with "<email>"
  And I fill in "password" with "<password>"
  And I press "Sign in"
  Then should see "<result>" in the "div" element
  Examples:
  | email | password | result |
  |    example2@example.pl   |    maciejmaciej      |   Invalid credentials.     |
  |    user@user.pl   |    user  |  Email could not be found.    |



  Scenario: login incorrect email
    Given I am on "/login"
    When I fill in "email" with "maciej@maciej.pl"
    And I fill in "password" with "maciejmaciej"
    And I press "Sign in"
    Then should be on "/"


