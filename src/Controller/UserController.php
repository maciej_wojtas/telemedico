<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\TokenService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Repository\UserRepository;
use App\Form\UserRegistrationType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\UserAuthToken;

class UserController extends AbstractController
{


    public function postUserAction(Request $request,
                                   UserPasswordEncoderInterface $passwordEncoder,
                                   EntityManagerInterface $em): JsonResponse
    {


        $form = $this->createForm(UserRegistrationType::class, null, ['csrf_protection' => false]);
        $data = $request->request->get('User');
         $form->submit($data);

        if ($form->isValid()) {
            $user = $form->getData();

            $user->setPassword($passwordEncoder->encodePassword($user, $form['plainPassword']->getData()));
            $userAuthToken = new UserAuthToken($user);
            $em->persist($user);
            $em->persist($userAuthToken);
            $em->flush();
            $location = $this->generateUrl('api_users_get', ['id' => $user->getId()]);

            return $this->json($user, 200, ['Content-Type' => 'application/json', 'Location' => $location], ['groups' => ['api']]);
        }
        $data = array(
            'type' => 'validation_error',
            'title' => "wrong data",
            'errors' => $form->getErrors(true, false),
        );


        return $this->json($data, 400, ['Content-Type' => 'application/json']);
    }


    public function getUsersAction(UserRepository $userRepo): JsonResponse
    {
        $users = $userRepo->findAll();
        if (!$users) {
            $data = array(
                'type' => 'not_found_error',
                'title' => "Collection 'users' not found",
            );
            return $this->json($data, 400, ['Content-Type' => 'application/json']);
        }
        return $this->json($users, 200, ['Content-Type' => 'application/json'], ['groups' => ['api']]);
    }


    public function getUserAction(int $id, UserRepository $userRepo): JsonResponse
    {
        $user = $userRepo->findOneBy(['id' => $id]);
        if (!$user) {
            $data = array(
                'type' => 'not_found_error',
                'title' => "user with  id:" . $id . "  doesn't exist",
            );
            return $this->json($data, 400, ['Content-Type' => 'application/json']);
        }
        return $this->json($user, 200, ['Content-Type' => 'application/json'], ['groups' => ['api']]);
    }


    public function putAction(int $id,
                              Request $request,
                              UserRepository $userRepo,
                              UserPasswordEncoderInterface $passwordEncoder,
                              EntityManagerInterface $em): JsonResponse
    {
        $user = $userRepo->findOneBy(['id' => $id]);

        if (!$user) {
            $data = array(
                'type' => 'not_found_error',
                'title' => "user with  id:" . $id . "  doesn't exist",
            );
            return $this->json($data, 400, ['Content-Type' => 'application/json']);
        }

        $form = $this->createForm(UserRegistrationType::class, $user, ['csrf_protection' => false]);
        $data = $request->request->get('User');

        $form->submit($data);

        if ($form->isValid()) {
            $user = $form->getData();

            $user->setPassword($passwordEncoder->encodePassword($user, $form['plainPassword']->getData()));
            $userAuthToken = new UserAuthToken($user);
            $em->persist($user);
            $em->persist($userAuthToken);
            $em->flush();
            $location = $this->generateUrl('api_users_get', ['id' => $id]);

            return $this->json($user, 200, ['Content-Type' => 'application/json', 'Location' => $location], ['groups' => ['api']]);
        }
        $data = array(
            'type' => 'validation_error',
            'title' => "wrong data",
            'errors' => $form->getErrors(true, false)
        );


        return $this->json($data, 400, ['Content-Type' => 'application/json']);
    }

    /**
     * @Route("/users/{id}", name="delete_user" , methods={"DELETE"})
     */
    public function deleteUserAction(int $id, UserRepository $userRepo, TokenService $tokenService): JsonResponse
    {

        $em = $this->getDoctrine()->getManager();
        $user = $userRepo->find($id);
        if (!$user) {
            $data = array(
                'type' => 'not_found_error',
                'title' => "user with  id:" . $id . "  doesn't exist",
            );
            return $this->json($data, 400, ['Content-Type' => 'application/json']);
        };
        $tokenService->removeAllUserTokens($user);
        $em->remove($user);
        $em->flush();
        return $this->json($user, 204, ['Content-Type' => 'application/json'], ['groups' => ['api']]);
    }

}
