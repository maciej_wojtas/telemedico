<?php

namespace spec\App\Entity;

use App\Entity\UserDetails;
use App\Entity\UserGroupA;
use App\Exception\NotAMaleException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class UserGroupASpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(UserGroupA::class);
    }

    function it_has_no_users_by_default()
    {
        $this->getUsers()->shouldHaveCount(0);
    }

    function it_should_be_able_to_add_users()
    {
        $this->addUser(new UserDetails());
        $this->addUser(new UserDetails());
        $this->getUsers()->shouldHaveCount(2);

    }

    function it_should_not_allow_to_add_females_to_males()
    {
        $this->addUser(new UserDetails());
        $this->shouldThrow(NotAMaleException::class)->during('addUser', [new UserDetails('red', 30, 'female'), true]);
    }
}
