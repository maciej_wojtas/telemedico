Do budowy aplikacji użyłem  symfony w wersji 4.3

Po skonfigurowaniu aplikacji, wykonaniu migracji oraz data fixtures, z poziomu użytkownika możliwe jest:
1. Utworzenie konta w aplikacji po poprawnym uzupełnieniu  formularza oraz przejściu walidacji. Po rejestracji automatyczne logowanie.
2. Zalogowanie się do aplikacji wraz z funkcją "remember me".

Wewnątrz aplikacji utworzyłem validator, który validuje użytkownika poprzez unikalny Token. Po poprawnej validacji daj użytkownikowi dostęp do zasobów users wg. metodologii REST. Pozwala na operacje CRUD oraz przyjmuje i zwraca dane w formacie JSON. W razie złego zapytania API zwraca informacje o błędach. Aby zaprezentować działanie API napisałem testy za pomocą PHP Unit.

aby przetestować API należy wykonać data fixtures:

w konsoli: ./bin/console doctrine:fixtures:load

a następnie uruchomić testy:

w konsoli: ./bin/phpunit Tests


Maciej Wojtas
