<?php

declare(strict_types=1);

namespace App\Entity;

class UserDetails
{
    private $color = 'undefined';

    private $age;

    private $sex;


    public function __construct(string $color = 'undefined', int $age = 0, $sex = 'male')
    {
        $this->color = $color;
        $this->age = $age;
        $this->sex = $sex;

    }

    public static function buildDefaultUser($sex): self
    {
        $userDetails = new static('red', 20);
        $userDetails->sex = $sex;
        return $userDetails;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    public function setAge(int $age): void
    {
        $this->age = $age;

    }

    public function getAge(): int
    {
        return $this->age;

    }

    public function getDescription()
    {
        return sprintf('Users color is %s and age is %d ',
            $this->color,
            $this->age ? $this->age : 'undefined'
        );
    }

    public function getSex(): string
    {
        return $this->sex;
    }

    public function setSex(string $sex): void
    {
        $this->sex = $sex;
    }

    public function isMale()
    {
        return $this->sex === 'male';
    }


}
