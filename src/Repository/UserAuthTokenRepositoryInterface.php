<?php

declare(strict_types=1);

namespace App\Repository;


interface UserAuthTokenRepositoryInterface
{
    public function getAllUserTokens(int $userId): array;
}
