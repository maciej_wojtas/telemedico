<?php

namespace spec\App\Factory;

use App\Factory\UserDetailsFactory;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use App\Entity\UserDetails;

class UserDetailsFactorySpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(UserDetailsFactory::class);
    }
    function it_creates_default_user_details(){
        $userDetails=$this->createMaleUserDetails();
        $userDetails->shouldBeAnInstanceOf(UserDetails::class);
        $userDetails->getColor()->shouldBeString();
        $userDetails->getColor()->shouldBe('red');
        $userDetails->getAge()->shouldBe(20);
        $userDetails->getAge()->shouldBeInteger();
        $userDetails->getSex()->shouldBe('male');
        $userDetails->getSex()->shouldBeString();
    }
}
