<?php
declare(strict_types=1);

namespace App\Entity;


use App\Exception\NotAMaleException;

class UserGroupA
{
    /** @var UserDetails[] */
    private $users = [];

    public function getUsers():array
    {
        return $this->users;
    }

    public function addUser(UserDetails $user):void
    {
        if(!$this->canAddUser($user)){
            throw new NotAMaleException();
        }
        $this->users[] = $user;
    }
    private function canAddUser($user){
        return count($this->users)== 0 || $this->users[0]->isMale() === $user->isMale();

    }
}
